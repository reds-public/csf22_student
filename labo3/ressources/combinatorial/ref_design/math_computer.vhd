library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity math_computer is
    generic (
        N        : integer := 3;
        DATASIZE : integer := 8;
        ERRNO    : integer := 0
        );
    port (
        a_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        b_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        c_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        valid_i  : in std_logic;
        ready_o  : out std_logic;
        result_o : out std_logic_vector(DATASIZE - 1 downto 0);
        ready_i  : in std_logic;
        valid_o  : out std_logic
        );
end math_computer;

`protect begin_protected
`protect version = 1
`protect encrypt_agent = "QuestaSim" , encrypt_agent_info = "2020.1_1"
`protect key_keyowner = "Mentor Graphics Corporation" , key_keyname = "MGC-VERIF-SIM-RSA-2"
`protect key_method = "rsa"
`protect encoding = ( enctype = "base64" , line_length = 64 , bytes = 256 )
`protect key_block
ie2j125UUGRM9YjuUqSbwCErALWH7jtLjnfeTLXqHqN9lNeMSge0IbDRMGDIYred
psjGvEf8jCKvEEdMNKoz8M/sg6c12qnR33jcSRrCxW1p66JQZ8jz1EgbDH9CiKJZ
UASk6zqzOyCPyll+JRamuvrrtpB1dey3doQKbu/jDm6ZjPAZkGNv2OIIE72yCNL6
M8hhsttPRaHtW58NpUneSG2BHgmR7G8Md+4vEZvADFAHe7f9xdYHVXtEsY0w9Bmp
6QwSW6mibRN72SMGb/Zga2aTtO7cZUllTfdyI7a7ve9OKX8KAu7Dhq5dtzAtzxde
HyINBlWdrETqH3RlFcYafA==
`protect data_method = "aes128-cbc"
`protect encoding = ( enctype = "base64" , line_length = 64 , bytes = 1392 )
`protect data_block
RaMLGZZoBMMQuJV8Mm4nnPegej4bKfRBMqIQc5NrWFu/WkFpJmh9bXZAeiKoQ0lX
UAG8/DnRrQJI6o9FUJoy310NH+v4k4lzMuPJ2Ve8t1xSakr/ykDEPqwYdlsBvvLD
J1b8pzyJEbCWNesqSZbGxJsODZ39VfoKlkzhiYWGHImzStXMUqTLV6qLW74LlVeY
dYUrEKzQqDJeDrLuJOYbQKin0AUQPN/h1HHmgGN/PQb/4oSY6PiauaBny/2fdSMu
tstrH8LgJrPSX1c7+i9sn0DCARpoBmQMOcXjwGYOG/Sbe3rlLzCEAW37ZmorrTtv
k1lTpyOyoTXo+Ifihch+97ZniQWJ8t98rzqC2RR3jBsmru4wwyifSGEBNBL1qW+0
iXSYn2XI5hLnPYh1VBhgSCSbDzUlJR7krPoz/aTZxfAuZsUFieRL+GqrpES8BDRy
ByiJU0EaZs7moTYjl60gXlKrGWrRfpiBnXxJ8tjOiMt5NmDxXKXIfV/bl2RHwOsl
jc/lzfyhlxAtfbzyuPUK2uXe2r0giOLdDfXB4PHDcfBYewkDd5VEjRs0kGRfRD3H
SyCFaM5z+RN8lOEmXZPiMsqmlQVF92P13rLiS6Gt5z0fqfjZH+V6yIhX8ycLSemk
AvvTw7mGzI93hVKZrAzwJ95Ft7CpnnvMifM8ZgB7oCVG+7TNTOKoBqdgmssRfSwp
5KVTn6aXNCtuRIwZFiN+jJQa+xpIMD1ya47y4VmATY5XlPq5EnsY22rkxzI4+uuK
/fFIOjrRVo4mmJKuYjcZylSVGQeeNK1Z9fRKGI6HcEmvBzSCPrhiQApzQwndnB1y
OpMw941dHFC7srYQdihK9wVQn3vr/kddoRM53E0r1zKZWBrA5g04KGbOP/Op3g6i
wCHfaW1md+nBd4PDmVC4IV2/ld+YQN0isLjfPUJq95xyNwFuH8y3hRMfkzWMVjX9
nNUpkgRtNYOLG23eMSs3/llTcCJXSiHan4du50U3zIuHikdVNn+JRItvPVMumn0h
ztmlcgwNt8/P6NWqeOGBxouYHYJf/WnsfPCyCzXxFUZIQyD0Wzk2oZt2yS7LhYUT
FAmNMKGT4qxGnd1wngpmFoNLXh/aXUviAg6yFoV7XFg7TfBuF34c2lRre/wsqyex
7s+cSHz4pqHir8NLdi6i6EfjwAXIqvd+6I6U8zpD+2qTFn4ZnM/zf6pUSjVa7Vwf
8WEJ7Vpq3A7h7KxLJ416NUoclE8ek6oOinWHzrR2LO+YMXV0TsWYi2jMXMdGZVgL
UP+tlkQ5RrKd/KAtne/i5XU7MK+ZLhLnzo8wpFOInuBXrp70WRUpnt+zU4fCXhg5
RBxoMpQwr8hgAdSL/8+h/9kZXf5MwQb9k0m+xx2Hh6hvrz7fXngfjEM5ny13Mx3O
4JPU4RV1FT5QPdxPlvIvPmb5FjuPq8mMIzakYMvx+4GnWiHTRcWwFi4C5faVmks6
w862iVp2Kw8YRKF1nvTOlYPPds52IxwDnwaBhOAGSEwDh22QwlCMQeWT2EUR61a6
Fq1cRsdbaPYobMpkjP4pQU/aZ2hvK/TuPi/jyjAkZ9YX4YL+c13CRfNVQkZ1DcOm
hJr0oj0oMLdQ1Kf9pHu4+i0Bava5nxnaiTp3WiaxmnOFu8O9iaqMVUSB8cOI3SY0
sarSsx7Ed2sDWNEOlzxtttY3/auTgPhtZMfhwEyIp9NjkjiAB3bkVf9lUSZTOiTb
DpX9IhS0DZMR9dld2To/FfEid9fKV9X5H408WKBnC3/cM8Tl5idsa70eQMdDsMSX
6pMxnw5vMNzf6yhld/iWRyu4OZ3ce+M9jFg35xwccHQn9dEhn9jVOvPiMR/IfK/5
`protect end_protected
