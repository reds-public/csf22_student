--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : math_computer_tb.vhd
-- Author   : TbGenerator
-- Date     : 02.03.2022
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   02.03.2022  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity math_computer_tb is
    generic (
        TESTCASE : integer := 0;
        N        : integer := 3;
        DATASIZE : integer := 8;
        ERRNO    : integer := 0
    );
    
end math_computer_tb;

architecture testbench of math_computer_tb is

    type stimulus_t is record
        a_i     : std_logic_vector(DATASIZE - 1 downto 0);
        b_i     : std_logic_vector(DATASIZE - 1 downto 0);
        c_i     : std_logic_vector(DATASIZE - 1 downto 0);
        valid_i : std_logic;
        ready_i : std_logic;
    end record;
    
    type observed_t is record
        ready_o  : std_logic;
        result_o : std_logic_vector(DATASIZE - 1 downto 0);
        valid_o  : std_logic;
    end record;
    

    signal stimulus_sti  : stimulus_t;
    signal observed_obs  : observed_t;
    signal reference_ref : observed_t;

    constant PERIOD : time := 10 ns;

    signal sim_end_s : boolean   := false;
    signal synchro_s : std_logic := '0';

    component math_computer is
    generic (
        N        : integer := 3;
        DATASIZE : integer := 8;
        ERRNO    : integer := 0
    );
    port (
        a_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        b_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        c_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        valid_i  : in std_logic;
        ready_o  : out std_logic;
        result_o : out std_logic_vector(DATASIZE - 1 downto 0);
        ready_i  : in std_logic;
        valid_o  : out std_logic
    );
    end component;
    

    procedure check(stimulus  : stimulus_t;
                    observed  : observed_t;
                    reference : observed_t) is
    begin
        -- TODO : do the check, maybe differently
        if (observed /= reference) then
            report "Error in check" severity error;
        end if;
    end check;

    procedure calculate_reference(stimulus : stimulus_t;
                                  reference : out observed_t) is
    begin
        -- TODO : calculate the reference

    end calculate_reference;


    procedure testcase0(signal synchro : in std_logic;
                        signal stimulus : out stimulus_t;
                        signal reference : out observed_t) is

        variable stimulus_v  : stimulus_t;
        variable reference_v : observed_t;

    begin
        for i in 0 to 999 loop
            wait until rising_edge(synchro);
            -- TODO : assign the stimulus_v variable
            -- stimulus_v.a_i     := default_value;
            -- stimulus_v.b_i     := default_value;
            -- stimulus_v.c_i     := default_value;
            -- stimulus_v.valid_i := default_value;
            -- stimulus_v.ready_i := default_value;
            

            stimulus <= stimulus_v;
            calculate_reference(stimulus_v, reference_v);
            reference <= reference_v;
        end loop;
    end testcase0;

begin

    duv : math_computer
    generic map (
        N        => N,
        DATASIZE => DATASIZE,
        ERRNO    => ERRNO
    )
    port map (
        a_i      => stimulus_sti.a_i,
        b_i      => stimulus_sti.b_i,
        c_i      => stimulus_sti.c_i,
        valid_i  => stimulus_sti.valid_i,
        ready_o  => observed_obs.ready_o,
        result_o => observed_obs.result_o,
        ready_i  => stimulus_sti.ready_i,
        valid_o  => observed_obs.valid_o
    );
    

    synchro_proc : process is
    begin
        while not(sim_end_s) loop
            synchro_s <= '0', '1' after PERIOD/2;
            wait for PERIOD;
        end loop;
        wait;
    end process;

    verif_proc : process is
    begin
        loop
            wait until falling_edge(synchro_s);
            check(stimulus_sti, observed_obs, reference_ref);
        end loop;
    end process;

    stimulus_proc: process is
    begin
        -- stimulus_sti.a_i     <= default_value;
        -- stimulus_sti.b_i     <= default_value;
        -- stimulus_sti.c_i     <= default_value;
        -- stimulus_sti.valid_i <= default_value;
        -- stimulus_sti.ready_i <= default_value;
        

        report "Running TESTCASE " & integer'image(TESTCASE) severity note;

        -- do something
        case TESTCASE is
            when 0      => -- default testcase

                testcase0(synchro_s, stimulus_sti, reference_ref);

            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
