--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : math_computer_tb.vhd
-- Author   : TbGenerator
-- Date     : 02.03.2022
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   02.03.2022  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity math_computer_tb is
    generic (
        TESTCASE : integer := 0;
        N        : integer := 3;
        DATASIZE : integer := 8;
        ERRNO    : integer := 0
    );
    
end math_computer_tb;

architecture testbench of math_computer_tb is

    signal a_sti      : std_logic_vector(DATASIZE - 1 downto 0);
    signal b_sti      : std_logic_vector(DATASIZE - 1 downto 0);
    signal c_sti      : std_logic_vector(DATASIZE - 1 downto 0);
    signal valid_sti  : std_logic;
    signal ready_obs  : std_logic;
    signal result_obs : std_logic_vector(DATASIZE - 1 downto 0);
    signal ready_sti  : std_logic;
    signal valid_obs  : std_logic;
    

    constant PERIOD : time := 10 ns;

    signal sim_end_s : boolean   := false;
    signal synchro_s : std_logic := '0';

    component math_computer is
    generic (
        N        : integer := 3;
        DATASIZE : integer := 8;
        ERRNO    : integer := 0
    );
    port (
        a_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        b_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        c_i      : in std_logic_vector(DATASIZE - 1 downto 0);
        valid_i  : in std_logic;
        ready_o  : out std_logic;
        result_o : out std_logic_vector(DATASIZE - 1 downto 0);
        ready_i  : in std_logic;
        valid_o  : out std_logic
    );
    end component;
    

begin

    duv : math_computer
    generic map (
        N        => N,
        DATASIZE => DATASIZE,
        ERRNO    => ERRNO
    )
    port map (
        a_i      => a_sti,
        b_i      => b_sti,
        c_i      => c_sti,
        valid_i  => valid_sti,
        ready_o  => ready_obs,
        result_o => result_obs,
        ready_i  => ready_sti,
        valid_o  => valid_obs
    );
    

    synchro_proc : process is
    begin
        while not(sim_end_s) loop
            synchro_s <= '0', '1' after PERIOD/2;
            wait for PERIOD;
        end loop;
        wait;
    end process;

    verif_proc : process is
    begin
        loop
            wait until falling_edge(synchro_s);
            -- TODO : check, by comparing expected result with real output values
        end loop;
    end process;

    stimulus_proc: process is
    begin
        -- a_sti      <= default_value;
                    -- b_sti      <= default_value;
                    -- c_sti      <= default_value;
                    -- valid_sti  <= default_value;
                    -- ready_sti  <= default_value;
                    

        report "Running TESTCASE " & integer'image(TESTCASE) severity note;

        -- do something
        case TESTCASE is
            when 0      => -- default testcase

                for i in 0 to 999 loop
                    wait until rising_edge(synchro_s);
                    -- TODO : stimulate
                    -- a_sti      <= default_value;
                    -- b_sti      <= default_value;
                    -- c_sti      <= default_value;
                    -- valid_sti  <= default_value;
                    -- ready_sti  <= default_value;
                    
                end loop;

            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
