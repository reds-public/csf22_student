library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity math_computer is
    generic (
        N        : integer := 3;
        DATASIZE : integer := 8;
        ERRNO    : integer := 0
        );
    port (
        clk_i    : in  std_logic;
        rst_i    : in  std_logic;
        a_i      : in  std_logic_vector(DATASIZE - 1 downto 0);
        b_i      : in  std_logic_vector(DATASIZE - 1 downto 0);
        c_i      : in  std_logic_vector(DATASIZE - 1 downto 0);
        valid_i  : in  std_logic;
        ready_o  : out std_logic;
        result_o : out std_logic_vector(DATASIZE - 1 downto 0);
        ready_i  : in  std_logic;
        valid_o  : out std_logic
        );
end math_computer;


`protect begin_protected
`protect version = 1
`protect encrypt_agent = "QuestaSim" , encrypt_agent_info = "2020.1_1"
`protect key_keyowner = "Mentor Graphics Corporation" , key_keyname = "MGC-VERIF-SIM-RSA-2"
`protect key_method = "rsa"
`protect encoding = ( enctype = "base64" , line_length = 64 , bytes = 256 )
`protect key_block
Rm1m9qZqmztAHfJB+wJM3QedV7ioWSG1Y8+R6Lph2iwwndH56So7c+dXwjU9yJpj
A8RiPeQYlrMxB8HfCGpAP7ZTLVBvRWPZt9F50VGggH6NDN64Xuy9m1ec0nhP6IZ5
d6zqRB7Chr0w2d3XVAQ/RnNgiHxyrvOIEP/2lIkNUUtjDJeOLJFSpCFigDOb74Ri
naFe052UUD/IVfE3V+UNcQBPOVqfUg0sjjHlxvfkVv4cZLI3JZjT75kUSuXQyWOp
Tc1UOEBxeeo0o2dc6GzdiRiJUxWwx2pHLgksaT00a0R7P6C3adqn/1Q/hDVYwOS7
PIq3v/SPNy5lzT2l9JjTGw==
`protect data_method = "aes128-cbc"
`protect encoding = ( enctype = "base64" , line_length = 64 , bytes = 5760 )
`protect data_block
6w19jBrwB9sVjsPSn2Nmn9LHSvLTDTpFdrPXeRDZ8VdJ5znqhBkwxmUz4Va+0Lyv
LS3WWKzpOqADsXUMVJyYAKIjau0aIiMVohz4rP6hIFHn/LPdxW23IwfrYWgbrzEc
cqn99ZwPTgPPZA4dfCsZFMbl7x+jWrha9FEk6WPoEpjlhgNHvOfKXYI2zpaPeK0p
T0485zcxEWw2Uy+/v1n1xwuS3SmTyl5qCSQPxpw/S+O9tR2vbDRNohPaFh4NE5ta
57j1LN7DFGntQRAsarvx8SyU8iM13DNZce5bPlUZ9BtpfFucyulPAPm9FJvlvpsR
C4WAyHxOn+BwNWCvudlE8S6bshbbEhG5GC++0lwl4MUp5nmXe+0Tt/MlH037mU39
qWO2gtKt8sojNVZy53jHjxdb7+BmFC8NcqsUM44eiSfp1JRfqiJd8g15xeT88MtQ
nw4k6Pvo3K6Csf85Jw0ZBqd2CU1Edj0H7Bd3sT6BN2ZhFV10xlxOIuPnVcpojO9i
zoBc7yUQ+Sgcj+V30FuV2VSu6jhCsbYVpBLnmu+aaYYlOz4a2w07jaFxgNfidTxA
D7pAoMwVK1K5ZTvN0XzIT3okSDfpy7u6qxde3niFLQ5fosupwbWnNEeAjLZVyVSf
9dVkuc0hquOAEpQUGP/miJ9UMekXdoQjQxj7XDCQeEERYsIMP+tisWRSYe8H7/G6
kdxHsxiVKLbXs09Wwv49hfuhBKAE6SZoNKDCUak+UIzZA6h0d9MkwnJXr/VtP4ln
2fali2bj4U6BguRz0+m4g9UxeuTNU3CGtQfGmT1n721GuWqRVfjEiV6nVTwz1NoX
n2BTL4Z/TO641rHlOUGzHwomoQ/QT62TJ4NG609PX5ppqfL703+W5uKQTFV1dGBk
mWb+Lva3BzNe9lBpF6WDIkY0EkCLJqB1O14CbwaE9huWVSoxniyMfqVmk4o+1UoM
+A5UCC2m5p9O6xP+Y4ang1NWk3ujSLGJE+8yH8JZtlh8u3tRtDwlVyXoeoToeDea
sz9piSx60TvNAEm5lF5O7WTlJ1++7xXrZ391L/JkzZmomQJTktG+H02UKrhgz3Kd
SVTYbf2WUSJRjfrtHYXA3GyGk2OFKTGIBiEJQcKb4PlXjWlmchF62FnJgtE7bCn8
pfxuozDAIQWpveQ8ySg8U4jaXzgLHGwTiCcLIGXTOn+cjforgueq7prjgfFA/ymO
fTKPrQhswKeLwfDcJekTNTwEsf9myNsU6dhkhu0f8j1aAx1/jC1PR70UWLEBCn4p
kbQ6VyCbKdLPDrK8lFXHb5mHu3FX0gXz2tVuxIS7rtc2dnw/dvj6Wv7c95Sk0BLh
lrbJP3hMZHHi78GPbsRb7kE97F8IF90XNiTGirU43KpF0TAh0OhWsEwHGKSFrMm7
lRzRnh/7cI491lskaorjLMnImNAoXxj5c3nGtbXR3pAShxskLVNWbssS2gOmTadF
cr9bVbIgJEIQjCF8aeV0Kze5WZ38sH+I6XVmHjeI3/jJ9+YUXAr9SEU7177w/HVF
x/WsPC7OTOvwiIKoM2V3yG8sJLnrdQEaBlJPaatxCfmJlLhighM3QXJBBvl5UTZ6
9OvAFmdjNxMpUr1L5eGQnZyh/U5jYRub1Y/yXb73AUwUzexG91OLn+PdNWIhwRo/
5+W/avlP3bWpN54Y5SpCb5OhqBCs11F8fRDmyJo722weEgyGyztWRg1Ye7n+hwg4
YA6gkytMl2qiRktJxiMM9WvWmUGg5nwsJ/iZlWBUeWYwoqhpRLMqqf6tIm+j8B/q
p5VIqbpuZ9lRFa/Kzcn5PKG3lagwchyow3A1b/Fk4m7+7Xv3VJzFyZWET2jrIF49
Y+pgYjolFRtgGmBqIhDibRZiuIHUrhQTGtm7mAcKt3XnTboBILWnzcMi9bihdGCf
h4dbvVfHAE88baNvqVlpmDzpy4ioGJyYxzVQL4iPlUrvQTZuH73p5To4PKis5XPW
gmW27pl4wu09FA+THrUVcNWCWfDpSit+biUnPk9MmZpbMmcIe42ocyOhtClnZXQ+
U5k4bumfEX4SlYQlyIoSH//ZrBH2qU71hHnc+spxPt3ZA0nqi7XfMaeMmTEL/nSV
anOz6+jQT31jqzETtQIbmvYUQvNYncj70bheB5lZop91sv6mxgG1dmHiOEDkKlAT
7ewlVfRmjySqeMuUzlTx1O4HigNA6BmFvDac6zIdNxlHsodQtSX9mShehG5qq74u
ifQLje647py6AArZjDqgpafez2E0MC0sS/MCKPKqznlt/I5Pv78OdM5DJR+JOd5W
nG1VkBKWjn10u+xLokPTpSmcRB5u5VAS57KKiC1ytZ2cM64oFEea2XsZ/DFUeh3B
z4LyY9g/yf1hb2nJjbBdlP3KvM7y21GfLkwvVugTntat1OYKUnQNc3WLTRxOc8b9
KdBTCOUbo5BQya/rBelvVEO+WIYCt823g0Yp1GDPc8V+/EHinva4voT9m3oCjzG3
xrOArislyWEC7ZHVpvIG/03dIUmQD9Qu3SsEqfklcZdjesW5s5LC7y/MoILw6z09
fvRgYNKUdMO9kaDj6C3tF7w/BVLFGroaeSndtFVa2allI9bf/MilJhp5mnDj7hgU
ZFtV/d7mlxuKGLJNii9kuhxIs53DBglYQaESxxgmUmejgAfXBFZ2qovX9M57XorG
6JXbARvizZuyTONoCCkbdDIJ+UVwqbKEOvg1/GXkDQ8EmjICD9M8M7kGIUmofc4S
+k0V1K5l6omJDIvhea3NmQytI2sNJNsrj7UazFTVdEUl9xe+r/wvbrVbwg+FxZZR
AwpWNEzPqCloSZbkvuLtPKUu3HoB+gDlZ8wa2CwCGFMYpuPG+geSgMviwayrJQ/J
Y2M5A2UDUmV7tI/BQOWIyxns8CeJrQsUmWqBmAFQ6joAOY1NZOqsa3OCNidqm6Of
25Pq8QXvpDGFohT6QoEi/TL+0BBk6O1PHKk18Rs2CoQ/GDpJs90ZNlXEsqoD2jd2
gnQIgQfN+L13Y7l9aESsr2hYZqYYn+BaxxqRkTPDxmBJrK27VYvAlM3B7D7hlEhR
pDx1tjKlgVQI5/O+OGspTV2BfCx3mEVvAnL3d+CW292YDmWS21X9DvBG8q9j+ciu
e7FjN/RcyCQd6H08QmnuhQYzoalXTYytYAeqaQO8i5qrwqf0SfYcUshpdR1gfT2O
zApielamuYgk5uTWgt0l15Jq6AlkQyIY7GyInA2grVpeNx98XtlsV3tLuzYCvHmi
bK7wNFE11OtWcDDQClem6ZROGOEGl3GWY8KYwtbQ1FzSdJeUfiPST+zxXnNszMkk
zbShSBXBsr/hU22UtPkWESNkmFdVE54qkvaCFS0FW4AGV1PkhlwKnqcQqA7smoQh
RV6EK5/+0LfKXd+QuT3aDAtHYKWG4eB2+i00Mf+uSpKfIY94RDnaGq+KOdLNllvd
qD6vzERtw0Uid0RL5jEWfPnCRYzMVzhAMI5Cue/R2HvVRDEhJmCFIYyC2EF8bCDi
32FpVbpYprqCiJXXAaSsHi1KX75kMn1wd7zIlthDNG0v5YsrrzWapEAM7uxP1117
SsKc2VxUt2gjGfCntdzn4reB1mX+ZHoeAd01LlXVOy9C+wQzWZ2Rm8PGZit+uzjy
MspxlM+CzAiSq4/Rb9HF4GqLqTOxthZaGGjZ4acJEysL8h0Yq474PtZiufYwN3Q/
IVywEDIWjpxNGYxGgXeVrO6oUcRmdd8OY2zEro2PsQ7FHn/ILhN4K8WYv/M/hhKx
T+gJlWhXCu9aMhhLPYKfrcl6c3eeTwCEC/rzCuXqwkXgtDjl1w5K9+DNMaYqexz3
hlWkjYW+VatMSP1+VHV8hVZpzbqotERX5JxIAq17oPygBpUrJSjN7RJEKk4SuaIv
AvW/junpO5S+koytQt37Gg1a84DidFsigcVneylfO0vY0Rif9MH4SCueTyjjc+bV
zclnJ54/OKDy7kBMYTBiMRP14vgR+19M6JkJxMbBVfheDiAmuYySdaOlnQqZWh0Y
kVCHFmfkhMnFu8nZcCbi6+8ytTMxuPaEZij3M/n0GO3YF7KUTby3CWlvHw1N8T8w
L36F25oY8hAXqb2Cr2h5jhx2ICPI5LtC2eGzy7OyNPmCeecCzcqiYs+Aso6BkZvw
WGE/CXhVP+tN5JDsJRqDXO/ks3Z2LF2RfQtEPEwJntbyi0n3Yn74SwPeAunwLz7U
YiuItQ7NIIUPom7ZJaFBc50IbGkpmPX7N9TqDST86qQRAqTs4VgNGc5tH7l+Hilp
g0+F1ICm/rXl+xccKiR3X27fZ7/9yeO+XqpYWx6SicPFuuLlth288LGkiz6pq02q
8p7nKWTsy4089XzuL+3oM6bKqUOQ6cMsp6nA0Y2NsrHQDEIXtdB6LymfTjW60BFa
S49JqiV94Q8ta7V6FUhwGfYwaTJwGxUihWzhyriMHJEt3GSWTp6DPSYS2Quq8n4S
Kb8ePe6oRiMjJ1helExFx9AFyPm1UFnZgGwlsgX5fxgJs6fu77dYUHvM7+DLb0GJ
e2U9a2SZchUVIoGa32uJDZR/Scph3JdhLeLg+IEnirQhWqZPxxCEjrNdqCakfbgf
+rrN8xEMzFN8JibJFQCr9xhW0c7ouPc4G92XJ1RQiOPS82C9zyryCHCQQ6Hms1yG
fiuDimL+NTaa99h8OVq12srhP6xoxH4hfAOY5HPalvRMd5FhG0jvijXYJl0OTSHX
4VbZKZE3DyLvW/9ydoH8+iNDChTRcHxxC4EownTqLa6YT8TMTyh8dHRuKX+H6vbg
wNxwxESpfoenTyxujf5y3zOC28a9OpgNqGQrw8SxSYCMalkrVQrISg0+FznW4L1R
bb8X5oQ7rRTyAjvHYs58gIUzGs1969Il0qYlmzbwHVkwI/o55hAY/j367wCvmu4X
bcUzwA84P5+2xGmmRVDCF2PvwAvaQ7a6tOFaIuGTpblNOku8loVODNgs3+ugVdQ7
DKS7k6W05dXTlEc6OrXV4KAHQMhmqU7j3wikMoaMFvEeU04/tzFbzmPngLg+RvXJ
0sajx8QCg6M+eFlpLznM1zjFBaK+hFZClGvNrWWM7alhEeHpQZyt3uuYOeGlE6gu
VJx/d/ab+ikm1EKAg/qHaOwGHxcfXYKReJGd2E6NYdzrVHuFDRcQoduRQUwNADWR
ONgb5QGv3j+nfFBc4QEF8Pt7LdffVA8EpQ1LuR9GxaE0H5rA/RqI3lRdPpGxRtzm
gmtDXWpxGxx6xCIMAFiH04IrSQc87nAWtylqyzjiDSbao1udYmmAjgo41/BG99E2
LdxJgKGz+A3omsqzPngu+4eaVkQm0SUHohZw2tMYbKh/1E8xP6w5U9EUHkp6LEIp
LjrrvusZS4XOVjE5EdTHC++7gnbVtkgVOS3HLc7NlC7IOIN2pCV2otTpPhvQTwXO
F/wqkT0z/rPNS2sCkrXPM0pipbw1xiFynpKSdVISz9Q9BwgI4nI564d0lparlxpP
hrW1KEuWXr58aam9Q2lpeEYdRpO3fILjSI0YsIqF+oXXOva/x7HrprX6QJpgftr0
IJxKtH+vCheZjnB9deJ3MR2ImVOg2nFk3cUUt54HFLgvoeAw3JZ7b6otYPHgdiKr
Wdbet+FjtEDdV5LtdgCq1TcmnuHYuYX0FFP/gsOymu54tuh9I9tucdzoDpVdReDc
AD3e4iogf0rm2nXGxUgpGqIRj4kIxzw5EVnpIOkZzT+jyTo0oWvZ6TrG+WFNSCAn
TbvNijaU/FcICeXdbmJ2CXfO8H/HCN1hL1zbJGQQ2E8p9hvxThPfp7CCRVqiAD+T
pMDLFhWpQWHE3vlissz2KGmI+daqzypLsOXImervQiPgCgRe/SHT5t3GuDrP+T1O
qraStyCLJ+LC2K31a+pT7qr/q8aKWRPPn+58imfRqnbqMqaiyLfIfEYX8zqEeTDE
pWZdFRIWJN5sylNQ3sjTFVn+5lR7FveX76+AXEhsb/JOt9ILAVhKN49BZQcas+bn
FxA5yHn0yt7XJmF6SRx3PbP5VRfIRysGcnS4jQRxjk3YvQfnd19BQ1VONMRozp80
GOTRG3+bGs2zVT/CCIWIPNw+Db3Fb6SiTFDhImNYZCUj4OrlSfouO6KMeRfLoEW2
hptn4o/oHhxAM57kaL1dHPmghqzHpk4SbyLn9JbuNRgxROZzYWk4PAZMVRL9wTXI
43+qKRpRpot/OEXgbF3hnmECUB59Z0+1pIiRut9qIGhvCjToeOZTT7Q7EFWKJC75
aTdo84cDJTYszDMD458s6ELffyAGHpeLPfyNZmGbIsbxMsfO9vhm0hK4q1HwmZWQ
cLGnwf8dYFmRxkXV+xUVVgEVnBLQiuS1rZKy4NdYtAxE8wu3kLfM09IrhoEAqBgH
ZZ4Eq7u/ic6Xo6WW0/FCcFx+n4d+JLQQUnMAfdw8jZ2B8fph6LXFQtYZOtHQy6kB
UubBWULQ6bCzJRU53x89xlQobjVekQuZwDDw8Q0Tj9P8EoBtfOYLI7opluC+ay/2
1ZCzHmJoRxcO/MxkdnEe+KJW6wAgyEPdKdFOScpEU6H2Skn3x0WVsgZP+h66mQ1D
yW2zqe9jjkxcO7B8J1RFUcqMe7470EVJ8OqGpZCOizdmcbhXRfiM8wjlcn3UrCGm
UBXfiZSF64cOI236TOmGHDXyg9Vmiac+rokxCgV+4VvrVbThx44OCyJRkR3sw+gp
gZEJ35xsYGaSvrUNBVs7RTlwEQA3PlzbhFUBm2rHKJyBBb76aPqrOQrf9rybY45o
pDr3y215DtZ8G/CyEQLtUwlLkXgAU7SlYxUK9a8xEi0zMzYZqP0i7a/FHsMBfAQW
8mI2lt4E6o/yfaiVFOioox9+6UFbiV999NQpXqGumZlF4ak3C2A0pizA+PW827Wb
KnlGj4/9cQaxbV9awVZsRZqpPLq2KBddNSkzfRK157OnbAjd1MjIL37DX/GdstoZ
7Z8+fLwu2jAOvDnAtcKAxyzfxFIktdkmGX2zASfqQcH9+Xwu4Jt/BIQIpdX6Egfs
W1MGPa2s/5oCqaBPlNCBJgEmErYKRjFfvrymVgdRQqa3Br5o/kZnSzu8E1qUSMvp
7AQfXkzLEaUl8P5OTRCFkS/oDy711ZY0ee1kR/5UgWBKc5tb9i4wcN5zfwAAVEXI
E/hI1gXRPftEqzF8pPDxpu8dN7WZL2iSa8OQJpKVHKrhnWk5D01QD3m7V46F+I1L
igo5qN4N8NJh+E0/9VXsOhvtXz90CGwR1TUz2LKgVFn6bJ8aS6jIViyppqQI1w/u
jraQk55UIAYoA5pxee0TGAy2KNNwW/Swt7IyJtgKah3puZhkVujnHILcRBtTJAOA
TXOrB2v4NgOqJSF0YpmUX4Q78tV2cOAAU6IRcy16dUOSlTp5OzRx8q8Pm8qRdsax
P8+yEZvBYb21SIYNfJO8wpaoWQINJBcvTM+ZAz23SOzGF8tW+ZPmNNaLq2RXMsn9
vxJSY045oFZoHgJE/MF3TXJYzJOZJseT0+dDySncN5rx/QvcNGxBZzNPkjnz6Py9
qHUywNbVXRQkub0eRJ8UgLvOUxvxV30kgkvJRuyfg2KjcE5nmrZVA833FeuN/5rl
9jRvsgDgU/17UMRLOiDdXZDtnAD31WIFrxb/0hU7sm2dY3DejV4alPoQ2JsyIP0V
`protect end_protected
