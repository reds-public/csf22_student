#!/usr/bin/tclsh

# Main proc at the end #

proc tlmvm_compil { } {

  set currentDir [pwd]

  cd ../tlmvm/comp
  do ../scripts/compile.do
  cd $currentDir

  vmap tlmvm ../tlmvm/comp/tlmvm
}

#------------------------------------------------------------------------------
proc vhdl_compil { } {
  global Path_VHDL
  global Path_TB
    
  #puts "\nREF compilation :"
  #vcom -2008 $Path_REF/*.vhd

  puts "\nVHDL compilation :"

  vcom -just p -2008 $Path_VHDL/*.vhd
  vcom -just pb -2008 $Path_VHDL/*.vhd
  vcom -just e -2008 $Path_VHDL/*.vhd
  vcom -just a -2008 $Path_VHDL/*.vhd
  vcom -just c -2008 $Path_VHDL/*.vhd
  vcom -2008 $Path_TB/random_pkg.vhd
  vcom -2008 $Path_TB/math_computer_verif_pkg.vhd
  vcom -2008 $Path_TB/math_computer_tb.vhd
}

#------------------------------------------------------------------------------
proc sim_start {N DATASIZE DUV ERRNO} {

  vsim -t 1ns -GDATASIZE=$DATASIZE -GN=$N -GDUV=$DUV -GERRNO=$ERRNO work.math_computer_tb
#  do wave.do
  add wave -r *
  wave refresh
  run -all
}

#------------------------------------------------------------------------------
proc do_all {N DATASIZE DUV ERRNO} {
  tlmvm_compil
  vhdl_compil
  sim_start $N $DATASIZE $DUV $ERRNO
}

## MAIN #######################################################################


if {$argc==1} {
  if {[string compare $1 "help"] == 0} {
    puts "Call this script with one of the following options:"
    puts "    all         : compiles and run, with 4 arguments (see below)"
    puts "    comp_vhdl   : compiles all the VHDL files"
    puts "    sim         : starts a simulation, with 4 arguments (see below)"
    puts "    tlmvm       : compiles the TLMVM library. To be done once"
    puts "    help        : prints this help"
    puts "    no argument : compiles and run with N=3, DATASIZE=8, sequential implementation, ERRNO=0"
    puts ""
    puts "When 4 arguments are required, the order is:"
    puts "    1: value of N for the computation"
    puts "    2: DATASIZE, so the size of data"
    puts "    3: 0 -> sequential design, 1 -> pipelined design"
    puts "    4: ERRNO value to be passed to instrumentalized DUV"
    exit
  }
}

# Compile folder ----------------------------------------------------
if {[file exists work] == 0} {
  vlib work
}

quietly set Path_VHDL     "../src_vhdl"
quietly set Path_TB       "../src_tb"
quietly set Path_REF      "../ref_design"

puts -nonewline "  Path_VHDL => "
puts $Path_VHDL
puts -nonewline "  Path_TB   => "
puts $Path_TB

global Path_VHDL
global Path_TB

# start of sequence -------------------------------------------------

if {$argc==1} {
  if {[string compare $1 "all"] == 0} {
    do_all $2 $3 $4 $5
  } elseif {[string compare $1 "comp_vhdl"] == 0} {
    vhdl_compil
  } elseif {[string compare $1 "sim"] == 0} {
    sim_start $2 $3 $4 $5
  } elseif {[string compare $1 "tlmvm"] == 0} {
    tlmvm_compil
  }

} else {
  do_all 3 8 0 0
}
