-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : math_computer_tb.vhd
--
-- Description  : Ce banc de test vérifie le bon fonctionnement d'un calculateur
--                séquentiel. Il faut évidemment le compléter
--
-- Auteur       : Yann Thoma
-- Date         : 29.03.2022
-- Version      : 1.0
--
-- Utilise      :
--              :
--
--| Modifications |------------------------------------------------------------
-- Version   Auteur      Date               Description
-- 1.0       YTA         see header         First version.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.RNG.all;

use work.math_computer_verif_pkg.all;

------------
-- Entity --
------------
entity math_computer_tb is
    generic (
        N        : integer := 3;
        DATASIZE : integer := 8;
        DUV      : integer := 0;
        ERRNO    : integer := 0
    );
end math_computer_tb;

------------------
-- Architecture --
------------------
architecture test_bench of math_computer_tb is


    subtype input_transaction_t is  math_computer_input_transaction_t(
                                     a(DATASIZE-1 downto 0),
                                     b(DATASIZE-1 downto 0),
                                     c(DATASIZE-1 downto 0));

    subtype output_transaction_t is
        math_computer_output_transaction_t (result(DATASIZE-1 downto 0));

    subtype input_t is  math_input_itf_in_t(
                                    a(DATASIZE-1 downto 0),
                                    b(DATASIZE-1 downto 0),
                                    c(DATASIZE-1 downto 0));

    subtype output_t is
        math_output_itf_out_t (result(DATASIZE-1 downto 0));


-- Create a specialized FIFO package with the same data width for holding
-- the input transactions.
-- Very important: Use the specialized subtypes, not the generic ones
    package input_transaction_pkg is new tlm_unbounded_fifo_pkg
        generic map (element_type => input_transaction_t);


    -- Create a specialized FIFO package with the same data width for holding
    -- the output transactions.
    -- Very important: Use the specialized subtypes, not the generic ones
    package output_transaction_pkg is new tlm_unbounded_fifo_pkg
        generic map (element_type => output_transaction_t);


    ---------------
    -- Constants --
    ---------------
    constant CLK_PERIOD : time := 10 ns;
    constant NUMBER_OF_TRANSACTIONS : integer := 100;

    ----------------
    -- Components --
    ----------------

    ----------------------
    -- Shared Variables --
    ---------------------- -- May be accessed by more than one process at the time
    shared variable fifo_sti : input_transaction_pkg.tlm_fifo_type; -- Stimuli transactions
    shared variable fifo_obs : output_transaction_pkg.tlm_fifo_type; -- Responses
    shared variable fifo_ref : input_transaction_pkg.tlm_fifo_type; -- Reference

    -------------
    -- Signals --
    -------------
    signal clk_sti      : std_logic;
    signal rst_sti      : std_logic;



    signal input_itf_in_sti   : input_t;
    signal input_itf_out_obs  : math_input_itf_out_t;
    signal output_itf_in_sti  : math_output_itf_in_t;
    signal output_itf_out_obs : output_t;


    type simulation_info_t is protected

        procedure new_input;

        procedure new_output;

        procedure add_error;

        procedure log_end;

    end protected simulation_info_t;

    type simulation_info_t is protected body

        variable nb_input  : integer := 0;
        variable nb_output : integer := 0;
        variable nb_error  : integer := 0;

        procedure new_input is
        begin
            nb_input := nb_input + 1;
        end new_input;

        procedure new_output is
        begin
            nb_output := nb_output + 1;
        end new_output;

        procedure add_error is
        begin
            nb_error := nb_error + 1;
        end add_error;

        procedure log_end is
        begin
            -- TODO : Add useful information

        end log_end;

    end protected body simulation_info_t;

    shared variable simulation_info : simulation_info_t;

    ----------------
    -- Procedures --
    ----------------
    procedure rep(status : finish_status_t) is
    begin
        simulation_info.log_end;
    end rep;

begin

    -- Simulation monitor
    monitor : simulation_monitor
    generic map (
        drain_time => 3000 ns, -- Timeout (objections)
        beat_time => 3000 ns, -- Timeout (heart beat)
        final_reporting => rep
    );

    -- Clock generation
    clock_generator(clk_sti, CLK_PERIOD);

    -- Reset generation
    simple_startup_reset(rst_sti, CLK_PERIOD * 10);

    gen_DUV : if DUV = 0 generate
    -- Device under test
    DUV : entity work.math_computer(sequential)
    generic map(
        N        => N,
        DATASIZE => DATASIZE,
        ERRNO    => ERRNO
    )
    port map(
        clk_i    => clk_sti,
        rst_i    => rst_sti,
        a_i      => input_itf_in_sti.a,
        b_i      =>input_itf_in_sti.b,
        c_i      => input_itf_in_sti.c,
        valid_i  => input_itf_in_sti.valid,
        ready_o  => input_itf_out_obs.ready,
        result_o => output_itf_out_obs.result,
        ready_i  => output_itf_in_sti.ready,
        valid_o  => output_itf_out_obs.valid
    );

    else generate
    -- Device under test
    DUV : entity work.math_computer(pipeline)
    generic map(
        N        => N,
        DATASIZE => DATASIZE,
        ERRNO    => ERRNO
    )
    port map(
        clk_i    => clk_sti,
        rst_i    => rst_sti,
        a_i      => input_itf_in_sti.a,
        b_i      =>input_itf_in_sti.b,
        c_i      => input_itf_in_sti.c,
        valid_i  => input_itf_in_sti.valid,
        ready_o  => input_itf_out_obs.ready,
        result_o => output_itf_out_obs.result,
        ready_i  => output_itf_in_sti.ready,
        valid_o  => output_itf_out_obs.valid
    );
    end generate;

    -- Driver process : will take transactions from the stimulation FIFO and
    -- administer them to the DUT
    driver_process : process is
        variable data_v : input_transaction_t;
    begin

        input_itf_in_sti.valid <= '0';
        input_itf_in_sti.a <= (others => '0');
        input_itf_in_sti.b <= (others => '0');
        input_itf_in_sti.c <= (others => '0');

        loop

            -- Get a word from the stimuli FIFO
            input_transaction_pkg.blocking_get(fifo_sti, data_v);

            -- TODO : Manage the driver

            simulation_info.new_input;

            wait until falling_edge(clk_sti);
        end loop;

    end process driver_process;

    -- Reader process : Once the DUT is full this process will read from the DUT
    -- forever, if the DUT is not empty this process will put the word read in
    -- the obs FIFO for review by the verification process
    reader_process : process is

        variable ok : boolean;

        variable output_transaction_v : output_transaction_t;
    begin
        output_itf_in_sti.ready <= '0';
            output_itf_in_sti.ready <= '1';

        loop

            wait until falling_edge(clk_sti);

            -- TODO : Manage output of the DUV

            raise_objection; -- Check usage of objections
            output_transaction_v.result := output_itf_out_obs.result;

            simulation_info.new_output;

            output_transaction_pkg.blocking_put(fifo_obs,output_transaction_v);

            drop_objection; -- Check usage of objections

            wait until rising_edge(clk_sti);
        end loop;
    end process reader_process;


    -- Stimulation process : This will generate data to the stimuli and
    -- reference FIFOs for the Driver to send to the DUT and for the
    -- verification process as a reference
    stimulation_process : process is
        variable stimulus : input_transaction_t;
    begin
        -- This will send a given number of data packets to the driver and the
        -- reference FIFO (for verification)

        for i in 1 to NUMBER_OF_TRANSACTIONS loop
            raise_objection; -- Check usage of objections

            stimulus.a := std_logic_vector(to_unsigned(i, stimulus.a'length));
            stimulus.b := std_logic_vector(to_unsigned(i+1, stimulus.a'length));
            stimulus.c := std_logic_vector(to_unsigned(i+2, stimulus.a'length));

            input_transaction_pkg.blocking_put(fifo_sti, stimulus);
            drop_objection; -- Check usage of objections
            input_transaction_pkg.blocking_put(fifo_ref, stimulus);
        end loop;

        wait;
    end process stimulation_process;

    -- Verification process : This will check the data reader from the DUT by
    -- the reader process and compare them in contents to the reference, this
    -- will also warn if more data has been read as expected, If the simulation
    -- ends without showing the message that all data was read there is a problem
    verification_process : process is
        variable data_obs_v  : output_transaction_t;
        variable data_ref_v  : input_transaction_t;
    begin
        -- This will check the data from the reader against the reference data

        loop
            output_transaction_pkg.blocking_get(fifo_obs, data_obs_v);
            input_transaction_pkg.blocking_get(fifo_ref, data_ref_v);
            -- TODO : Verify the data
        end loop;

    end process verification_process;

end test_bench;
