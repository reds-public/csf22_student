-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : math_computer_entity.vhd
-- Description  : 
--
-- Author       : 
-- Date         : 
-- Version      : 
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       TBD    xx.xx.xx           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------
-- Specifics libraries --
-------------------------

------------
-- Entity --
------------
entity math_computer is
    generic (
        N        : integer := 3;
        DATASIZE : integer := 8;
        ERRNO    : integer := 0
        );
    port (
        clk_i    : in  std_logic;
        rst_i    : in  std_logic;
        a_i      : in  std_logic_vector(DATASIZE - 1 downto 0);
        b_i      : in  std_logic_vector(DATASIZE - 1 downto 0);
        c_i      : in  std_logic_vector(DATASIZE - 1 downto 0);
        valid_i  : in  std_logic;
        ready_o  : out std_logic;
        result_o : out std_logic_vector(DATASIZE - 1 downto 0);
        ready_i  : in  std_logic;
        valid_o  : out std_logic
        );
end math_computer;
