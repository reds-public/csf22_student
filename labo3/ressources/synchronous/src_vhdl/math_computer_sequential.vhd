-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : math_computer_sequential.vhd
-- Description  : 
--
-- Author       : 
-- Date         : 
-- Version      : 
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       TBD    xx.xx.xx           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------
-- Specifics libraries --
-------------------------
library work;
use work.math_computer_pkg.all;

------------------
-- Architecture --
------------------
architecture sequential of math_computer is

    signal datapath_to_control_s : datapath_to_control_t;
    signal control_to_datapath_s : control_to_datapath_t;

begin

    ---------------------
    -- Control section --
    ---------------------
    CTRL_inst : entity work.math_computer_control
        port map (
            -- standard inputs
            clk_i    => clk_i,
            rst_i    => rst_i,
            -- Input record
            record_i => datapath_to_control_s,
            -- Output record
            record_o => control_to_datapath_s
            );

    ----------------------
    -- Datapath section --
    ----------------------
    DATAPATH_inst : entity work.math_computer_datapath
        port map (
            -- standard inputs
            clk_i    => clk_i,
            rst_i    => rst_i,
            -- Input record
            record_i => control_to_datapath_s,
            -- Output record
            record_o => datapath_to_control_s
            );

end architecture;

