-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : math_computer_pkg.vhd
-- Description  : 
--
-- Author       : 
-- Date         : 
-- Version      : 
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       TBD    xx.xx.xx           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------
-- Specifics libraries --
-------------------------

-------------------------
-- Package Declaration --
-------------------------
package math_computer_pkg is

    type control_to_datapath_t is record
        example : std_logic;
    end record;

    type datapath_to_control_t is record
        example : std_logic;
    end record;

end package math_computer_pkg;

------------------
-- Package Body --
------------------
package body math_computer_pkg is

    -- If needed
    
end package body;

