-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : math_computer_datapath.vhd
-- Description  : 
--
-- Author       : 
-- Date         : 
-- Version      : 
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       TBD    xx.xx.xx           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------
-- Specifics libraries --
-------------------------
library work;
use work.math_computer_pkg.all;

------------
-- Entity --
------------
entity math_computer_datapath is
    port (
        -- standard inputs
        clk_i    : in  std_logic;
        rst_i    : in  std_logic;
        -- Input record
        record_i : in  control_to_datapath_t;
        -- Output record
        record_o : out datapath_to_control_t
        );
end entity;  -- math_computer_datapath

------------------
-- Architecture --
------------------
architecture behave of math_computer_datapath is
begin
end architecture;

