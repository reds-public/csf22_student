#!/bin/bash

PROJECT_DIR_COMB=ressources/combinatorial
PROJECT_DIR_SYNC=ressources/synchronous
REPORT_FILE=rapport.pdf
ARCHIVE=rendu.tar.gz

if [ ! -d "$PROJECT_DIR_COMB" ]
then
    echo "Could not find $PROJECT_DIR_COMB_TB directory in $(pwd)" >&2
    exit 1
fi

if [ ! -d "$PROJECT_DIR_SYNC" ]
then
    echo "Could not find $PROJECT_DIR_SYNC directory in $(pwd)" >&2
    exit 1
fi

if [ ! -d "$PROJECT_DIR_SYNC" ]
then
    echo "Could not find $PROJECT_DIR_COMB_TB directory in $(pwd)" >&2
    exit 1
fi

if [ ! -f "$REPORT_FILE" ]
then
    echo "Could not find project file : $REPORT_FILE" >&2
    exit 1
fi

echo "The following files are archived in $ARCHIVE : "
tar --exclude='rendu.tar.gz' --exclude='*.o' --exclude='comp' --exclude='sim' --exclude='synth' -czvf $ARCHIVE $PROJECT_DIR_COMB $PROJECT_DIR_SYNC
