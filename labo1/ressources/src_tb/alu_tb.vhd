-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Composant    : alu_tb
-- Description  : Banc de test pour une unité arithmétique et logique
--                capable d'effectuer 8 opérations.
--                Un paramètre générique permet de définir la taille
--                des opérandes.
--                Il n'y a pas de vérification automatique implémentée
-- Auteur       : Yann Thoma
-- Date         : 28.02.2012
-- Version      : 0.2
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   23.02.2017  YTA        Cosmetic changes
-- 0.2   06.02.2018  YTA        Adding a generic parameter
-----------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu_tb is
    generic(
        SIZE : integer := 8
    );
end alu_tb;

architecture testbench of alu_tb is

    component alu is
    generic (
        SIZE  : integer := 8
    );
    port (
        a_i      : in  std_logic_vector(SIZE-1 downto 0);
        b_i      : in  std_logic_vector(SIZE-1 downto 0);
        result_o : out std_logic_vector(SIZE-1 downto 0);
        carry_i  : in  std_logic;
        carry_o  : out std_logic;
        mode_i   : in  std_logic_vector(2 downto 0)
    );
    end component;


    signal a_sti      : std_logic_vector(SIZE-1 downto 0);
    signal b_sti      : std_logic_vector(SIZE-1 downto 0);
    signal mode_sti   : std_logic_vector(2 downto 0);
    signal carry_sti  : std_logic;

    signal result_obs : std_logic_vector(SIZE-1 downto 0);
    signal carry_obs  : std_logic;

begin

    duv: alu
    generic map (
        SIZE => SIZE
    )
    port map(
        a_i      => a_sti,
        b_i      => b_sti,
        carry_i  => carry_sti,
        result_o => result_obs,
        carry_o  => carry_obs,
        mode_i   => mode_sti
    );

    process
        variable c: std_logic;
    begin
        for a in 0 to 2**SIZE-1 loop
            for b in 0 to 2**SIZE-1 loop
                for c in 0 to 1 loop
                    for mode in 0 to 7 loop
                        a_sti     <= std_logic_vector(to_unsigned(a,SIZE));
                        b_sti     <= std_logic_vector(to_unsigned(b,SIZE));
                        carry_sti <= to_unsigned(c,1)(0);
                        mode_sti  <= std_logic_vector(to_unsigned(mode,3));
                        wait for 10 ns;
                    end loop;
                end loop;
            end loop;
        end loop;
        wait;
    end process;

end testbench;
