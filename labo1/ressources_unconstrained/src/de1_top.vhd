-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : de1_top.vhd
-- Description  :
--
-- Author       : Rick Wertenbroek
-- Date         : 10.02.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RWE    10.02.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity de1_top is
    port (
        -- Entrées
        clk_i          : in std_logic;
        switches_i     : in std_logic_vector(9 downto 0);
        push_buttons_i : in std_logic_vector(3 downto 0); -- 0 quand appuyé

        -- Sorties

        -- Segments (anode commune)
        segments_0_o   : out std_logic_vector(6 downto 0);
        segments_1_o   : out std_logic_vector(6 downto 0);
        segments_2_o   : out std_logic_vector(6 downto 0);
        segments_3_o   : out std_logic_vector(6 downto 0);
        segments_4_o   : out std_logic_vector(6 downto 0);
        segments_5_o   : out std_logic_vector(6 downto 0);

        -- LEDs
        leds_o         : out std_logic_vector(9 downto 0)
    );
end de1_top;

architecture struct of de1_top is
    -----------
    -- Types --
    -----------
    subtype nibble is std_logic_vector(3 downto 0);
    subtype segment is std_logic_vector(6 downto 0);
    subtype eight_bit_word is std_logic_vector(7 downto 0);
    type value_array is array (5 downto 0) of nibble;
    type segment_array is array (5 downto 0) of segment;

    ---------------
    -- Registres --
    ---------------
    signal a_reg_s : eight_bit_word;
    signal b_reg_s : eight_bit_word;
    signal r_reg_s : eight_bit_word;
    signal carry_reg_s : std_logic;

    -------------
    -- Signaux --
    -------------
    signal rst_s      : std_logic;
    signal carry_s    : std_logic;
    signal result_s   : eight_bit_word;
    signal value_s    : value_array;
    signal segments_s : segment_array;

begin

    -- Câblage
    rst_s <= not push_buttons_i(0);

    value_s(0) <= r_reg_s(3 downto 0); -- Low Nibble
    value_s(1) <= r_reg_s(7 downto 4); -- High Nibble
    value_s(2) <= b_reg_s(3 downto 0);
    value_s(3) <= b_reg_s(7 downto 4);
    value_s(4) <= a_reg_s(3 downto 0);
    value_s(5) <= a_reg_s(7 downto 4);

    -- Câblage de l'ALU
    alu_1 : entity work.alu
        port map (
            a_i      => a_reg_s,
            b_i      => b_reg_s,
            result_o => result_s,
            carry_i  => switches_i(9),
            carry_o  => carry_s,
            mode_i   => switches_i(2 downto 0)
        );

    -- Process de mise à jour des registres
    maj_registres_process : process(clk_i) is
    begin

        if rising_edge(clk_i) then
            if rst_s = '1' then
                a_reg_s <= (others => '0');
                b_reg_s <= (others => '0');
                r_reg_s <= (others => '0');
                carry_reg_s <= '0';
            else
                -- Affiche résultat
                if push_buttons_i(1) = '0' then
                    r_reg_s <= result_s;
                    carry_reg_s <= carry_s;
                end if;

                -- Enregistre B
                if push_buttons_i(2) = '0' then
                    b_reg_s <= switches_i(7 downto 0);
                end if;

                -- Enregistre A
                if push_buttons_i(3) = '0' then
                    a_reg_s <= switches_i(7 downto 0);
                end if;
            end if;
        end if;

    end process maj_registres_process;

    -- Generate des décodeurs 7 segments
    dec_gen : for i in 0 to 5 generate
    begin
        decodeur : entity work.hex_7_seg
            port map (
                value_i    => value_s(i),
                enable_i   => '1',
                segments_o => segments_s(i)
            );
    end generate;

    -- Assignation des sorties

    -- Anode commune, allumé à l'état bas
    segments_0_o <= not segments_s(0);
    segments_1_o <= not segments_s(1);
    segments_2_o <= not segments_s(2);
    segments_3_o <= not segments_s(3);
    segments_4_o <= not segments_s(4);
    segments_5_o <= not segments_s(5);

    leds_o(9 downto 1) <= (others => '0'); -- Off
    leds_o(0) <= carry_reg_s;
end struct;
