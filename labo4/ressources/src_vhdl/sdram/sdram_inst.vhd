component sdram is
    port (
        avalon_slave_address       : in    std_logic_vector(24 downto 0) := (others => 'X');  -- address
        avalon_slave_byteenable_n  : in    std_logic_vector(1 downto 0)  := (others => 'X');  -- byteenable_n
        avalon_slave_chipselect    : in    std_logic                     := 'X';  -- chipselect
        avalon_slave_writedata     : in    std_logic_vector(15 downto 0) := (others => 'X');  -- writedata
        avalon_slave_read_n        : in    std_logic                     := 'X';  -- read_n
        avalon_slave_write_n       : in    std_logic                     := 'X';  -- write_n
        avalon_slave_readdata      : out   std_logic_vector(15 downto 0);  -- readdata
        avalon_slave_readdatavalid : out   std_logic;  -- readdatavalid
        avalon_slave_waitrequest   : out   std_logic;  -- waitrequest
        clk_clk                    : in    std_logic                     := 'X';  -- clk
        reset_reset_n              : in    std_logic                     := 'X';  -- reset_n
        sdram_interface_addr       : out   std_logic_vector(12 downto 0);  -- addr
        sdram_interface_ba         : out   std_logic_vector(1 downto 0);  -- ba
        sdram_interface_cas_n      : out   std_logic;  -- cas_n
        sdram_interface_cke        : out   std_logic;  -- cke
        sdram_interface_cs_n       : out   std_logic;  -- cs_n
        sdram_interface_dq         : inout std_logic_vector(15 downto 0) := (others => 'X');  -- dq
        sdram_interface_dqm        : out   std_logic_vector(1 downto 0);  -- dqm
        sdram_interface_ras_n      : out   std_logic;  -- ras_n
        sdram_interface_we_n       : out   std_logic   -- we_n
        );
end component sdram;

u0 : component sdram
    port map (
        avalon_slave_address       => CONNECTED_TO_avalon_slave_address,  --    avalon_slave.address
        avalon_slave_byteenable_n  => CONNECTED_TO_avalon_slave_byteenable_n,  --                .byteenable_n
        avalon_slave_chipselect    => CONNECTED_TO_avalon_slave_chipselect,  --                .chipselect
        avalon_slave_writedata     => CONNECTED_TO_avalon_slave_writedata,  --                .writedata
        avalon_slave_read_n        => CONNECTED_TO_avalon_slave_read_n,  --                .read_n
        avalon_slave_write_n       => CONNECTED_TO_avalon_slave_write_n,  --                .write_n
        avalon_slave_readdata      => CONNECTED_TO_avalon_slave_readdata,  --                .readdata
        avalon_slave_readdatavalid => CONNECTED_TO_avalon_slave_readdatavalid,  --                .readdatavalid
        avalon_slave_waitrequest   => CONNECTED_TO_avalon_slave_waitrequest,  --                .waitrequest
        clk_clk                    => CONNECTED_TO_clk_clk,  --             clk.clk
        reset_reset_n              => CONNECTED_TO_reset_reset_n,  --           reset.reset_n
        sdram_interface_addr       => CONNECTED_TO_sdram_interface_addr,  -- sdram_interface.addr
        sdram_interface_ba         => CONNECTED_TO_sdram_interface_ba,  --                .ba
        sdram_interface_cas_n      => CONNECTED_TO_sdram_interface_cas_n,  --                .cas_n
        sdram_interface_cke        => CONNECTED_TO_sdram_interface_cke,  --                .cke
        sdram_interface_cs_n       => CONNECTED_TO_sdram_interface_cs_n,  --                .cs_n
        sdram_interface_dq         => CONNECTED_TO_sdram_interface_dq,  --                .dq
        sdram_interface_dqm        => CONNECTED_TO_sdram_interface_dqm,  --                .dqm
        sdram_interface_ras_n      => CONNECTED_TO_sdram_interface_ras_n,  --                .ras_n
        sdram_interface_we_n       => CONNECTED_TO_sdram_interface_we_n  --                .we_n
        );

