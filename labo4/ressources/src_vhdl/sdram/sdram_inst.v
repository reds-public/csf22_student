	sdram u0 (
		.avalon_slave_address       (<connected-to-avalon_slave_address>),       //    avalon_slave.address
		.avalon_slave_byteenable_n  (<connected-to-avalon_slave_byteenable_n>),  //                .byteenable_n
		.avalon_slave_chipselect    (<connected-to-avalon_slave_chipselect>),    //                .chipselect
		.avalon_slave_writedata     (<connected-to-avalon_slave_writedata>),     //                .writedata
		.avalon_slave_read_n        (<connected-to-avalon_slave_read_n>),        //                .read_n
		.avalon_slave_write_n       (<connected-to-avalon_slave_write_n>),       //                .write_n
		.avalon_slave_readdata      (<connected-to-avalon_slave_readdata>),      //                .readdata
		.avalon_slave_readdatavalid (<connected-to-avalon_slave_readdatavalid>), //                .readdatavalid
		.avalon_slave_waitrequest   (<connected-to-avalon_slave_waitrequest>),   //                .waitrequest
		.clk_clk                    (<connected-to-clk_clk>),                    //             clk.clk
		.reset_reset_n              (<connected-to-reset_reset_n>),              //           reset.reset_n
		.sdram_interface_addr       (<connected-to-sdram_interface_addr>),       // sdram_interface.addr
		.sdram_interface_ba         (<connected-to-sdram_interface_ba>),         //                .ba
		.sdram_interface_cas_n      (<connected-to-sdram_interface_cas_n>),      //                .cas_n
		.sdram_interface_cke        (<connected-to-sdram_interface_cke>),        //                .cke
		.sdram_interface_cs_n       (<connected-to-sdram_interface_cs_n>),       //                .cs_n
		.sdram_interface_dq         (<connected-to-sdram_interface_dq>),         //                .dq
		.sdram_interface_dqm        (<connected-to-sdram_interface_dqm>),        //                .dqm
		.sdram_interface_ras_n      (<connected-to-sdram_interface_ras_n>),      //                .ras_n
		.sdram_interface_we_n       (<connected-to-sdram_interface_we_n>)        //                .we_n
	);

