
module sdram (
	avalon_slave_address,
	avalon_slave_byteenable_n,
	avalon_slave_chipselect,
	avalon_slave_writedata,
	avalon_slave_read_n,
	avalon_slave_write_n,
	avalon_slave_readdata,
	avalon_slave_readdatavalid,
	avalon_slave_waitrequest,
	clk_clk,
	reset_reset_n,
	sdram_interface_addr,
	sdram_interface_ba,
	sdram_interface_cas_n,
	sdram_interface_cke,
	sdram_interface_cs_n,
	sdram_interface_dq,
	sdram_interface_dqm,
	sdram_interface_ras_n,
	sdram_interface_we_n);	

	input	[24:0]	avalon_slave_address;
	input	[1:0]	avalon_slave_byteenable_n;
	input		avalon_slave_chipselect;
	input	[15:0]	avalon_slave_writedata;
	input		avalon_slave_read_n;
	input		avalon_slave_write_n;
	output	[15:0]	avalon_slave_readdata;
	output		avalon_slave_readdatavalid;
	output		avalon_slave_waitrequest;
	input		clk_clk;
	input		reset_reset_n;
	output	[12:0]	sdram_interface_addr;
	output	[1:0]	sdram_interface_ba;
	output		sdram_interface_cas_n;
	output		sdram_interface_cke;
	output		sdram_interface_cs_n;
	inout	[15:0]	sdram_interface_dq;
	output	[1:0]	sdram_interface_dqm;
	output		sdram_interface_ras_n;
	output		sdram_interface_we_n;
endmodule
