-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : display.vhd
-- Description  : mandelbrot
--
-- Author       : Mike Meury
-- Date         : 07.03.2018
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    07.03.2018         Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-----------------------
-- Project libraries --
-----------------------
library work;
use work.log_pkg.all;

------------
-- Entity --
------------
entity display is
    port (
        -- standard inputs
        clk_i           : in  std_logic;
        rst_i           : in  std_logic;
        -- SDRAM interface
        sdram_rd_o      : out std_logic;
        data_i          : in  std_logic_vector(15 downto 0);
        rst_addr_o      : out std_logic;
        request_i       : in  std_logic;
        last_pixel_i    : in  std_logic;
        -- VGA interface
        vga_red_o       : out std_logic_vector(7 downto 0);
        vga_green_o     : out std_logic_vector(7 downto 0);
        vga_blue_o      : out std_logic_vector(7 downto 0);
        vga_current_x_i : in  std_logic_vector(10 downto 0);
        vga_current_y_i : in  std_logic_vector(10 downto 0)
        );
end entity;

------------------
-- Architecture --
------------------
architecture behave of display is

    ---------------
    -- Constants --
    ---------------
    constant VGA_X_RANGE : integer := 1280;
    constant VGA_Y_RANGE : integer := 1024;

    -----------------------
    -- Internals signals --
    -----------------------
    signal data_s : std_logic_vector(15 downto 0);

begin

    -----------------------------------------------
    -- Synchronize read in sdram and VGA sweeper --
    -----------------------------------------------
    -- end of screen
    rst_addr_o <= last_pixel_i;
    sdram_rd_o <= request_i;
    --data_s     <= "00000000000" & vga_current_y_i(10 downto 6) when unsigned(vga_current_x_i) >= 500 else
    --              "00000" & vga_current_y_i(10 downto 5) & "00000"; --data_i;
    data_s     <= data_i;
    -- data_s <= X"F800" when ((unsigned(vga_current_x_i) > 800) and (unsigned(vga_current_y_i) > 800)) else
    --           X"001F" when ((unsigned(vga_current_x_i) < 300) and (unsigned(vga_current_y_i) < 500)) else
    --           X"07E0";


    ----------------------------
    -- Convert data as RGB565 --
    ----------------------------
    vga_red_o   <= data_s(15 downto 11) & "000";
    vga_green_o <= data_s(10 downto 5) & "00";
    vga_blue_o  <= data_s(4 downto 0) & "000";

end architecture;  -- behave
