	component pll_sdram is
		port (
			clk_clk                            : in  std_logic := 'X'; -- clk
			reset_reset_n                      : in  std_logic := 'X'; -- reset_n
			sys_sdram_pll_0_ref_clk_clk        : in  std_logic := 'X'; -- clk
			sys_sdram_pll_0_ref_reset_reset    : in  std_logic := 'X'; -- reset
			sys_sdram_pll_0_sys_clk_clk        : out std_logic;        -- clk
			sys_sdram_pll_0_sdram_clk_clk      : out std_logic;        -- clk
			sys_sdram_pll_0_reset_source_reset : out std_logic         -- reset
		);
	end component pll_sdram;

	u0 : component pll_sdram
		port map (
			clk_clk                            => CONNECTED_TO_clk_clk,                            --                          clk.clk
			reset_reset_n                      => CONNECTED_TO_reset_reset_n,                      --                        reset.reset_n
			sys_sdram_pll_0_ref_clk_clk        => CONNECTED_TO_sys_sdram_pll_0_ref_clk_clk,        --      sys_sdram_pll_0_ref_clk.clk
			sys_sdram_pll_0_ref_reset_reset    => CONNECTED_TO_sys_sdram_pll_0_ref_reset_reset,    --    sys_sdram_pll_0_ref_reset.reset
			sys_sdram_pll_0_sys_clk_clk        => CONNECTED_TO_sys_sdram_pll_0_sys_clk_clk,        --      sys_sdram_pll_0_sys_clk.clk
			sys_sdram_pll_0_sdram_clk_clk      => CONNECTED_TO_sys_sdram_pll_0_sdram_clk_clk,      --    sys_sdram_pll_0_sdram_clk.clk
			sys_sdram_pll_0_reset_source_reset => CONNECTED_TO_sys_sdram_pll_0_reset_source_reset  -- sys_sdram_pll_0_reset_source.reset
		);

