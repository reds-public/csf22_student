
module pll_sdram (
	clk_clk,
	reset_reset_n,
	sys_sdram_pll_0_ref_clk_clk,
	sys_sdram_pll_0_ref_reset_reset,
	sys_sdram_pll_0_sys_clk_clk,
	sys_sdram_pll_0_sdram_clk_clk,
	sys_sdram_pll_0_reset_source_reset);	

	input		clk_clk;
	input		reset_reset_n;
	input		sys_sdram_pll_0_ref_clk_clk;
	input		sys_sdram_pll_0_ref_reset_reset;
	output		sys_sdram_pll_0_sys_clk_clk;
	output		sys_sdram_pll_0_sdram_clk_clk;
	output		sys_sdram_pll_0_reset_source_reset;
endmodule
