	pll_sdram u0 (
		.clk_clk                            (<connected-to-clk_clk>),                            //                          clk.clk
		.reset_reset_n                      (<connected-to-reset_reset_n>),                      //                        reset.reset_n
		.sys_sdram_pll_0_ref_clk_clk        (<connected-to-sys_sdram_pll_0_ref_clk_clk>),        //      sys_sdram_pll_0_ref_clk.clk
		.sys_sdram_pll_0_ref_reset_reset    (<connected-to-sys_sdram_pll_0_ref_reset_reset>),    //    sys_sdram_pll_0_ref_reset.reset
		.sys_sdram_pll_0_sys_clk_clk        (<connected-to-sys_sdram_pll_0_sys_clk_clk>),        //      sys_sdram_pll_0_sys_clk.clk
		.sys_sdram_pll_0_sdram_clk_clk      (<connected-to-sys_sdram_pll_0_sdram_clk_clk>),      //    sys_sdram_pll_0_sdram_clk.clk
		.sys_sdram_pll_0_reset_source_reset (<connected-to-sys_sdram_pll_0_reset_source_reset>)  // sys_sdram_pll_0_reset_source.reset
	);

