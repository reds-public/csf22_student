-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : start_system.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 13.03.18
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    13.03.18           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity start_system is
    port (
        clk_i          : in  std_logic;
        rst_i          : in  std_logic;
        system_ready_o : out std_logic
        );
end entity;

architecture behave of start_system is

    signal cpt : unsigned(15 downto 0);

begin

    process(clk_i, rst_i)
    begin
        if rst_i = '1' then
            cpt            <= (others => '0');
            system_ready_o <= '0';
        elsif rising_edge(clk_i) then
            if cpt < 30000 then
                cpt            <= cpt + 1;
                system_ready_o <= '0';
            else
                system_ready_o <= '1';
            end if;
        end if;
    end process;

end architecture;
