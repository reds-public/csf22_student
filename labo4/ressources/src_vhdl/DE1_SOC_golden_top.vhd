------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : DE1_SOC_golden_top.vhd
-- Author               : Mike Meury (MIM)
-- Date                 : 09.02.2018
--
-- Context              : Pixel art with SDRAM
--
------------------------------------------------------------------------------------------
-- Description : TODO
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 1.0    08.03.18    MIM           Creation
--
------------------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

---------------------
-- Project library --
---------------------
library work;
use work.log_pkg.all;

------------
-- Entity --
------------
entity DE1_SOC_golden_top is
    port (
        -- Clocks
        CLOCK_50    : in    std_logic;
        TD_CLK27    : in    std_logic;
        -- USER interface
        GPIO_0      : inout std_logic_vector(35 downto 0);
        GPIO_1      : inout std_logic_vector(35 downto 0);
        HEX0        : out   std_logic_vector(6 downto 0);
        HEX1        : out   std_logic_vector(6 downto 0);
        HEX2        : out   std_logic_vector(6 downto 0);
        HEX3        : out   std_logic_vector(6 downto 0);
        HEX4        : out   std_logic_vector(6 downto 0);
        HEX5        : out   std_logic_vector(6 downto 0);
        KEY         : in    std_logic_vector(3 downto 0);
        LEDR        : out   std_logic_vector(9 downto 0);
        SW          : in    std_logic_vector(9 downto 0);
        -- VGA interface
        VGA_B       : out   std_logic_vector(7 downto 0);
        VGA_BLANK_N : out   std_logic;
        VGA_CLK     : out   std_logic;
        VGA_G       : out   std_logic_vector(7 downto 0);
        VGA_HS      : out   std_logic;
        VGA_R       : out   std_logic_vector(7 downto 0);
        VGA_SYNC_N  : out   std_logic;
        VGA_VS      : out   std_logic;
        -- SDRAM interface
        DRAM_ADDR   : out   std_logic_vector(12 downto 0);
        DRAM_BA     : out   std_logic_vector(1 downto 0);
        DRAM_CAS_N  : out   std_logic;
        DRAM_CKE    : out   std_logic;
        DRAM_CLK    : out   std_logic;
        DRAM_CS_N   : out   std_logic;
        DRAM_DQ     : inout std_logic_vector(15 downto 0);
        DRAM_LDQM   : out   std_logic;
        DRAM_RAS_N  : out   std_logic;
        DRAM_UDQM   : out   std_logic;
        DRAM_WE_N   : out   std_logic
        );
end entity;  -- DE1_SOC_golden_top

architecture rtl of DE1_SOC_golden_top is

    ----------------
    -- Components --
    ----------------
    component pll_sdram is
        port (
            clk_clk                            : in  std_logic := 'X';  -- clk
            reset_reset_n                      : in  std_logic := 'X';  -- reset_n
            sys_sdram_pll_0_ref_clk_clk        : in  std_logic := 'X';  -- clk
            sys_sdram_pll_0_ref_reset_reset    : in  std_logic := 'X';  -- reset
            sys_sdram_pll_0_sys_clk_clk        : out std_logic;         -- clk
            sys_sdram_pll_0_sdram_clk_clk      : out std_logic;         -- clk
            sys_sdram_pll_0_reset_source_reset : out std_logic  -- reset
            );
    end component pll_sdram;

    -----------------------
    -- Internals signals --
    -----------------------
    -- User I/O wrapping
    alias user_n_rst_a            : std_logic is SW(9);
    alias user_n_keys_a           : std_logic_vector(3 downto 0) is KEY(3 downto 0);
    alias user_switchs_a          : std_logic_vector(8 downto 0) is SW(8 downto 0);
    alias user_leds_a             : std_logic_vector(9 downto 0) is LEDR(9 downto 0);
    alias user_n_sevseg5_a        : std_logic_vector(6 downto 0) is HEX5;
    alias user_n_sevseg4_a        : std_logic_vector(6 downto 0) is HEX4;
    alias user_n_sevseg3_a        : std_logic_vector(6 downto 0) is HEX3;
    alias user_n_sevseg2_a        : std_logic_vector(6 downto 0) is HEX2;
    alias user_n_sevseg1_a        : std_logic_vector(6 downto 0) is HEX1;
    alias user_n_sevseg0_a        : std_logic_vector(6 downto 0) is HEX0;
    -- user signals
    signal user_rst_s             : std_logic;
    signal user_keys_s            : std_logic_vector(3 downto 0);
    signal user_keys_d1_s         : std_logic_vector(3 downto 0);
    signal user_keys_d2_s         : std_logic_vector(3 downto 0);
    signal user_sevseg5_s         : std_logic_vector(6 downto 0);
    signal user_sevseg4_s         : std_logic_vector(6 downto 0);
    signal user_sevseg3_s         : std_logic_vector(6 downto 0);
    signal user_sevseg2_s         : std_logic_vector(6 downto 0);
    signal user_sevseg1_s         : std_logic_vector(6 downto 0);
    signal user_sevseg0_s         : std_logic_vector(6 downto 0);
    -- specifics signals
    signal clk_pll_s              : std_logic;
    signal clk_sdram_s            : std_logic;
    signal data_to_sdram_s        : std_logic_vector(15 downto 0);
    signal wr_sdram_s             : std_logic;
    signal reset_wr_addr_s        : std_logic;
    signal data_from_sdram_s      : std_logic_vector(15 downto 0);
    signal data_read_valid_s      : std_logic;
    signal rd_sdram_s             : std_logic;
    signal reset_rd_addr_s        : std_logic;
    signal DRAM_DQM_s             : std_logic_vector(1 downto 0);
    signal vga_red_s              : std_logic_vector(7 downto 0);
    signal vga_blue_s             : std_logic_vector(7 downto 0);
    signal vga_green_s            : std_logic_vector(7 downto 0);
    signal vga_current_x_s        : std_logic_vector(10 downto 0);
    signal vga_current_y_s        : std_logic_vector(10 downto 0);
    signal ready_s                : std_logic;
    signal start_s                : std_logic;
    signal finished_s             : std_logic;
    signal max_iter_s             : std_logic_vector(15 downto 0);
    signal c_real_s               : std_logic_vector(15 downto 0);
    signal c_imaginary_s          : std_logic_vector(15 downto 0);
    signal z_real_s               : std_logic_vector(15 downto 0);
    signal z_imaginary_s          : std_logic_vector(15 downto 0);
    signal nb_iter_s              : std_logic_vector(15 downto 0);
    -- fifo counts
    signal wr_fifo_count_s        : std_logic_vector(12 downto 0);
    signal rd_fifo_count_s        : std_logic_vector(12 downto 0);
    signal data_to_display_7seg_s : std_logic_vector(23 downto 0);
    signal rd_fifo_sdram_s        : std_logic;
    -- reset system
    signal rst_sys_s              : std_logic;
    -- tests
    signal rd_sdram_test_s        : std_logic;
    signal wr_sdram_test_s        : std_logic;
    -- sync sw
    signal sw_sync, sw_sync2      : std_logic_vector(1 downto 0);
    signal sw_rose                : std_logic_vector(1 downto 0);
    signal sdram_ready_s          : std_logic;
    signal addr_wr_s              : std_logic_vector(24 downto 0);
    signal addr_rd_s              : std_logic_vector(24 downto 0);

    signal app_wdata_s : std_logic_vector(15 downto 0);
    signal app_rdata_s : std_logic_vector(15 downto 0);
    signal app_wen_s   : std_logic;
    signal app_ren_s   : std_logic;
    signal app_empty_s : std_logic;
    signal app_full_s  : std_logic;
    signal app_end_s   : std_logic;

    signal start_compute_s : std_logic;

    signal request_s : std_logic;

    signal last_pixel_s : std_logic;

begin

    --------------------
    -- Unused Outputs --
    --------------------
    GPIO_0         <= (others => 'Z');
    GPIO_1         <= (others => 'Z');
    user_sevseg0_s <= (others => '1');
    user_sevseg1_s <= (others => '1');
    user_sevseg2_s <= (others => '1');
    user_sevseg3_s <= (others => '1');
    user_sevseg4_s <= (others => '1');
    user_sevseg5_s <= (others => '1');
    user_leds_a    <= (others => '1');

    -----------------
    -- User Access --
    -----------------
    --user_rst_s       <= not user_n_rst_a;
    user_keys_s      <= not user_n_keys_a;
    user_n_sevseg0_a <= not user_sevseg0_s;
    user_n_sevseg1_a <= not user_sevseg1_s;
    user_n_sevseg2_a <= not user_sevseg2_s;
    user_n_sevseg3_a <= not user_sevseg3_s;
    user_n_sevseg4_a <= not user_sevseg4_s;
    user_n_sevseg5_a <= not user_sevseg5_s;

    ---------
    -- PLL --
    ---------
    pll_inst : pll_sdram
        port map (
            clk_clk                            => CLOCK_50,
            reset_reset_n                      => user_n_rst_a,
            sys_sdram_pll_0_ref_clk_clk        => CLOCK_50,
            sys_sdram_pll_0_ref_reset_reset    => user_rst_s,
            sys_sdram_pll_0_sys_clk_clk        => clk_pll_s,
            sys_sdram_pll_0_sdram_clk_clk      => clk_sdram_s,
            sys_sdram_pll_0_reset_source_reset => rst_sys_s
            );

    DRAM_CLK <= clk_sdram_s;

    ----------------------------
    -- System start for SDRAM --
    ----------------------------
    start_sys : entity work.start_system
        port map (
            clk_i          => clk_pll_s,
            rst_i          => rst_sys_s,
            system_ready_o => sdram_ready_s
            );

    ----------------------
    -- SDRAM controller --
    ----------------------
    SDRAM_INTERFACE : entity work.Votre_super_interface
        port map (  ------------------------------------------------------------------------
            -- standard inputs
            clk_i         => clk_pll_s,
            rst_i         => rst_sys_s,
            sdram_ready_i => sdram_ready_s,

            -- Vos supers IO

            ------------------------------------------------------------------------
            -- SDRAM interface -- Physical interface (Exemple)
            sdram_interface_addr  => DRAM_ADDR,
            sdram_interface_ba    => DRAM_BA,
            sdram_interface_cas_n => DRAM_CAS_N,
            sdram_interface_cke   => DRAM_CKE,
            sdram_interface_cs_n  => DRAM_CS_N,
            sdram_interface_dq    => DRAM_DQ,
            sdram_interface_dqm   => DRAM_DQM_s,
            sdram_interface_ras_n => DRAM_RAS_N,
            sdram_interface_we_n  => DRAM_WE_N
            );

    -- adapt for top level entity
    DRAM_UDQM <= DRAM_DQM_s(1);
    DRAM_LDQM <= DRAM_DQM_s(0);

    -----------------
    -- VGA display --
    -----------------
    display_inst : entity work.display
        port map (
            -- standard inputs
            clk_i           => clk_pll_s,
            rst_i           => rst_sys_s,
            -- Interface
            sdram_rd_o      => rd_sdram_s,
            data_i          => data_from_sdram_s,
            rst_addr_o      => reset_rd_addr_s,
            request_i       => request_s,
            last_pixel_i    => last_pixel_s,
            -- VGA interface
            vga_red_o       => vga_red_s,
            vga_green_o     => vga_green_s,
            vga_blue_o      => vga_blue_s,
            vga_current_x_i => vga_current_x_s,
            vga_current_y_i => vga_current_y_s
            );

    -------------------
    -- VGA controller --
    -------------------
    vga_ctrl_inst : entity work.vga_ctrl
        port map (
            -- Standard inputs
            clk_i        => clk_pll_s,  -- allow screen 1200x1024 (cf manuel)
            rst_i        => rst_sys_s,
            -- host side
            red_i        => vga_red_s,
            green_i      => vga_green_s,
            blue_i       => vga_blue_s,
            current_x_o  => vga_current_x_s,
            current_y_o  => vga_current_y_s,
            address_o    => open,
            request_o    => request_s,
            last_pixel_o => last_pixel_s,
            -- vga side
            vga_r_o      => VGA_R,
            vga_g_o      => VGA_G,
            vga_b_o      => VGA_B,
            vga_HS_o     => VGA_HS,
            vga_VS_o     => VGA_VS,
            vga_sync_o   => VGA_SYNC_N,
            vga_blank_o  => VGA_BLANK_N,
            vga_clock_o  => VGA_CLK
            );


    process(rst_sys_s, clk_pll_s)
    begin
        if rst_sys_s = '1' then
            user_keys_d1_s <= (others => '0');
            user_keys_d2_s <= (others => '0');
        elsif rising_edge(clk_pll_s) then
            user_keys_d1_s <= user_keys_s;
            user_keys_d2_s <= user_keys_d1_s;
        end if;
    end process;

    start_compute_s <= user_keys_d1_s(0) and not user_keys_d2_s(0);

end architecture;  -- rtl
