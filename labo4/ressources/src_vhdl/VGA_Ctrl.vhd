-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : VGA_Ctrl.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 07.03.18
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    07.03.18           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity VGA_Ctrl is
    port (
        -- Standard inputs
        clk_i        : in  std_logic;
        rst_i        : in  std_logic;
        -- host side
        red_i        : in  std_logic_vector(7 downto 0);
        green_i      : in  std_logic_vector(7 downto 0);
        blue_i       : in  std_logic_vector(7 downto 0);
        current_x_o  : out std_logic_vector(10 downto 0);
        current_y_o  : out std_logic_vector(10 downto 0);
        address_o    : out std_logic_vector(21 downto 0);
        request_o    : out std_logic;
        last_pixel_o : out std_logic;
        -- vga side
        vga_r_o      : out std_logic_vector(7 downto 0);
        vga_g_o      : out std_logic_vector(7 downto 0);
        vga_b_o      : out std_logic_vector(7 downto 0);
        vga_HS_o     : out std_logic;
        vga_VS_o     : out std_logic;
        vga_sync_o   : out std_logic;
        vga_blank_o  : out std_logic;
        vga_clock_o  : out std_logic
        );
end entity;

architecture behave of VGA_Ctrl is

    ---------------
    -- Constants --
    ---------------
    ------------------
    -- CLOCK 100MHz --
    ------------------
    -- Find on : http://tinyvga.com/vga-timing/1280x1024@60Hz
    -- Horizontal
    constant H_FRONT : unsigned(10 downto 0) := to_unsigned(48, 11);
    constant H_SYNC  : unsigned(10 downto 0) := to_unsigned(112, 11);
    constant H_BACK  : unsigned(10 downto 0) := to_unsigned(248, 11);
    constant H_ACT   : unsigned(10 downto 0) := to_unsigned(1280, 11);
    constant H_BLANK : unsigned(10 downto 0) := to_unsigned(408, 11);
    constant H_TOTAL : unsigned(10 downto 0) := to_unsigned(1688, 11);
    -- Vertical
    constant V_FRONT : unsigned(10 downto 0) := to_unsigned(1, 11);
    constant V_SYNC  : unsigned(10 downto 0) := to_unsigned(3, 11);
    constant V_BACK  : unsigned(10 downto 0) := to_unsigned(38, 11);
    constant V_ACT   : unsigned(10 downto 0) := to_unsigned(1024, 11);
    constant V_BLANK : unsigned(10 downto 0) := to_unsigned(42, 11);
    constant V_TOTAL : unsigned(10 downto 0) := to_unsigned(1066, 11);

    -----------------
    -- CLOCK 50MHZ --
    -----------------
    -- for 800x600
    -- Horizontal
    --constant H_FRONT : unsigned(10 downto 0) := to_unsigned(56, 11);
    --constant H_SYNC  : unsigned(10 downto 0) := to_unsigned(120, 11);
    --constant H_BACK  : unsigned(10 downto 0) := to_unsigned(64, 11);
    --constant H_ACT   : unsigned(10 downto 0) := to_unsigned(800, 11);
    --constant H_BLANK : unsigned(10 downto 0) := to_unsigned(240, 11);
    --constant H_TOTAL : unsigned(10 downto 0) := to_unsigned(1040, 11);
    -- Vertical
    --constant V_FRONT : unsigned(10 downto 0) := to_unsigned(37, 11);
    --constant V_SYNC  : unsigned(10 downto 0) := to_unsigned(6, 11);
    --constant V_BACK  : unsigned(10 downto 0) := to_unsigned(23, 11);
    --constant V_ACT   : unsigned(10 downto 0) := to_unsigned(600, 11);
    --constant V_BLANK : unsigned(10 downto 0) := to_unsigned(66, 11);
    --constant V_TOTAL : unsigned(10 downto 0) := to_unsigned(666, 11);

    -----------------------
    -- Internals signals --
    -----------------------
    -- counters
    signal counter_horizontal_s : unsigned(10 downto 0);
    signal counter_vertical_s   : unsigned(10 downto 0);
    -- Horizontal synchro
    signal HS_s                 : std_logic;
    signal set_HS_s, clear_HS_s : std_logic;
    -- Vertical synchro
    signal VS_s                 : std_logic;
    signal set_VS_s, clear_VS_s : std_logic;
    -- Blank
    signal H_count_less_H_blank : std_logic;
    signal V_count_less_V_blank : std_logic;

    -- Output registers
    signal vga_r_s     : std_logic_vector(7 downto 0);
    signal vga_g_s     : std_logic_vector(7 downto 0);
    signal vga_b_s     : std_logic_vector(7 downto 0);
    signal vga_HS_s    : std_logic;
    signal vga_VS_s    : std_logic;
    signal vga_sync_s  : std_logic;
    signal vga_blank_s : std_logic;

begin  -- behave

    -- This pin is unused
    vga_sync_s <= '1';

    -- VGA clock is our clock inversed
    vga_clock_o <= clk_i;

    -- red component
    vga_r_s <= red_i;

    -- green component
    vga_g_s <= green_i;

    -- blue component
    vga_b_s <= blue_i;

    -- Hs
    vga_HS_s <= HS_s;

    -- Vs
    vga_VS_s <= VS_s;

    -- Blank
    H_count_less_H_blank <= '1' when counter_horizontal_s < H_BLANK else
                            '0';
    V_count_less_V_blank <= '1' when counter_vertical_s < V_BLANK else
                            '0';
    vga_blank_s <= not (H_count_less_H_blank or V_count_less_V_blank);

    -- Current X
    current_x_o <= std_logic_vector(counter_horizontal_s-H_BLANK) when counter_horizontal_s >= H_BLANK else
                   (others => '0');

    -- Current Y
    current_y_o <= std_logic_vector(counter_vertical_s-V_BLANK) when counter_vertical_s >= V_BLANK else
                   (others => '0');

    -- Address (not used)
    address_o <= (others => '0');

    -- request -- Alignment considering fifo is NOT FWFT
    request_o <= '1' when (
        (counter_horizontal_s >= H_BLANK-1) and
        (counter_horizontal_s < H_TOTAL-1) and
        (counter_vertical_s >= V_BLANK-1) and
        (counter_vertical_s < V_TOTAL-1)
        ) else
                 '0';

    last_pixel_o <= '1' when ((counter_horizontal_s = H_TOTAL-1) and (counter_vertical_s = V_TOTAL-1)) else
                    '0';

    ------------------------
    -- Horizontal counter --
    ------------------------
    process (clk_i, rst_i)
    begin
        if rst_i = '1' then
            counter_horizontal_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if counter_horizontal_s < H_TOTAL-1 then
                -- we can add 1
                counter_horizontal_s <= counter_horizontal_s + 1;
            else
                -- we must reset (end of line)
                counter_horizontal_s <= (others => '0');
            end if;
        end if;
    end process;

    ----------------------
    -- Vertical counter --
    ----------------------
    process (clk_i, rst_i)
    begin
        if rst_i = '1' then
            counter_vertical_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if counter_horizontal_s = H_TOTAL-1 then
                if counter_vertical_s = V_TOTAL-1 then
                    counter_vertical_s <= (others => '0');
                else
                    counter_vertical_s <= counter_vertical_s + 1;
                end if;
            end if;
        end if;
    end process;

    ------------------------------
    -- Horizontal sync register --
    ------------------------------
    -- combinatorial logic
    set_HS_s <= '1' when counter_horizontal_s = H_FRONT+H_SYNC-1 else
                '0';
    clear_HS_s <= '1' when counter_horizontal_s = H_FRONT-1 else
                  '0';
    -- register
    process (clk_i, rst_i)
    begin
        if rst_i = '1' then
            HS_s <= '0';
        elsif rising_edge(clk_i) then
            if set_HS_s = '1' then
                HS_s <= '1';
            elsif clear_HS_s = '1' then
                HS_s <= '0';
            end if;
        end if;
    end process;

    ----------------------------
    -- Vertical sync register --
    ----------------------------
    -- combinatorial logic
    set_VS_s <= '1' when counter_vertical_s = (V_FRONT+V_SYNC-1) else
                '0';
    clear_VS_s <= '1' when counter_vertical_s = V_FRONT-1 else
                  '0';
    -- register
    process(clk_i, rst_i)
    begin
        if rst_i = '1' then
            VS_s <= '0';
        elsif rising_edge(clk_i) then
            if set_VS_s = '1' then
                VS_s <= '1';
            elsif clear_VS_s = '1' then
                VS_s <= '0';
            end if;
        end if;
    end process;

    ----------------------
    -- output registers --
    ----------------------
    process(clk_i, rst_i)
    begin
        if rst_i = '1' then
            vga_r_o     <= (others => '0');
            vga_g_o     <= (others => '0');
            vga_b_o     <= (others => '0');
            vga_HS_o    <= '0';
            vga_VS_o    <= '0';
            vga_sync_o  <= '0';
            vga_blank_o <= '0';
        elsif rising_edge(clk_i) then
            vga_r_o     <= vga_r_s;
            vga_g_o     <= vga_g_s;
            vga_b_o     <= vga_b_s;
            vga_HS_o    <= vga_HS_s;
            vga_VS_o    <= vga_VS_s;
            vga_sync_o  <= vga_sync_s;
            vga_blank_o <= vga_blank_s;
        end if;
    end process;

end architecture;  -- behave
