set NumericStdNoWarnings 1

file delete -force "work"
file mkdir "work"

vlib work
vmap work work

# Copy init files
cp ../init_sdram/sdram_new_sdram_controller_0_test_component.dat .

# design files
vlog ../src_vhdl/sdram/synthesis/submodules/altera_reset_controller.v
vlog ../src_vhdl/sdram/synthesis/submodules/altera_reset_synchronizer.v
vlog ../src_vhdl/sdram/synthesis/submodules/sdram_new_sdram_controller_0.v
vlog ../src_vhdl/sdram/synthesis/submodules/sdram_new_sdram_controller_0_test_component.v
vcom -2008 -explicit ../src_vhdl/sdram/simulation/sdram.vhd
vcom -2008 -explicit ../src_vhdl/VGA_Ctrl.vhd
vcom -2008 -explicit ../src_vhdl/display.vhd
vcom -2008 -explicit ../src_vhdl/start_system.vhd

# test bench files
vcom -2008 -explicit ../src_tb/sdram_itf_tb.vhd

#vsim  -t 1ps sdram_itf_tb
vsim -voptargs="+acc" -t 1ps sdram_itf_tb

add wave -r *

set StdArithNoWarnings 1
set NumericStdNoWarnings 1

run 10 ns

set StdArithNoWarnings 1
set NumericStdNoWarnings 1

run -all
