-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : sdram_itf_tb.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 16.05.22
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    16.05.22           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------

entity sdram_itf_tb is

end entity sdram_itf_tb;

-------------------------------------------------------------------------------

architecture test_bench of sdram_itf_tb is

    -- constants
    constant CLOCK_PERIOD : time := 10 ns;

    ---------------
    -- Component --
    ---------------
    component sdram_new_sdram_controller_0_test_component is
        port (
            clk      : in    std_logic;
            zs_addr  : in    std_logic_vector(12 downto 0);
            zs_ba    : in    std_logic_vector(1 downto 0);
            zs_cas_n : in    std_logic;
            zs_cke   : in    std_logic;
            zs_cs_n  : in    std_logic;
            zs_dqm   : in    std_logic_vector(1 downto 0);
            zs_ras_n : in    std_logic;
            zs_we_n  : in    std_logic;
            zs_dq    : inout std_logic_vector(15 downto 0)
            );
    end component;

    -- end of simulation
    signal end_sim : boolean := false;

begin  -- architecture test_bench

    -- component instantiation
    DUT : entity work.Votre_super_interface
        port map (
            clk_i         => clk_sti,
            rst_i         => rst_sti,
            sdram_ready_i => sdram_ready_sti,

            -- Vos super IO

            sdram_interface_addr  => sdram_interface_addr,
            sdram_interface_ba    => sdram_interface_ba,
            sdram_interface_cas_n => sdram_interface_cas_n,
            sdram_interface_cke   => sdram_interface_cke,
            sdram_interface_cs_n  => sdram_interface_cs_n,
            sdram_interface_dq    => sdram_interface_dq,
            sdram_interface_dqm   => sdram_interface_dqm,
            sdram_interface_ras_n => sdram_interface_ras_n,
            sdram_interface_we_n  => sdram_interface_we_n
            );

    -- Ceci est le modèle pour la simulation SDRAM, à conserver
    sdram_model : sdram_new_sdram_controller_0_test_component
        port map (
            clk      => clk_sti,
            zs_addr  => sdram_interface_addr,
            zs_ba    => sdram_interface_ba,
            zs_cas_n => sdram_interface_cas_n,
            zs_cke   => sdram_interface_cke,
            zs_cs_n  => sdram_interface_cs_n,
            zs_dqm   => sdram_interface_dqm,
            zs_ras_n => sdram_interface_ras_n,
            zs_we_n  => sdram_interface_we_n,
            zs_dq    => sdram_interface_dq
            );
    
    start : entity work.start_system
        port map (
            clk_i          => clk_sti,
            rst_i          => rst_sti,
            system_ready_o => sdram_ready_sti
            );

    ----------------------
    -- Clock generation --
    ----------------------
    clk_gen : process
    begin
        clk_sti <= '1';
        wait for CLOCK_PERIOD/2;
        clk_sti <= '0';
        wait for CLOCK_PERIOD/2;
        if end_sim = true then
            wait;
        end if;
    end process;

    --------------------
    -- Reset sequence --
    --------------------
    rst_gen : process
    begin
        rst_sti <= '1';
        wait for 5*CLOCK_PERIOD;
        rst_sti <= '0';
        wait;
    end process;

    ---------------------
    -- Wave generation --
    ---------------------
    wave_gen : process
    begin
        wait for 50 ms;                 -- cut simulation ending
        -- end of simulation
        end_sim <= true;
        wait;
    end process;

    data_sti <= std_logic_vector(value);

end architecture test_bench;

-------------------------------------------------------------------------------

configuration sdram_itf_tb_test_bench_cfg of sdram_itf_tb is
    for test_bench
    end for;
end sdram_itf_tb_test_bench_cfg;

-------------------------------------------------------------------------------
