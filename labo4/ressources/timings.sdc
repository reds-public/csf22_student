
create_clock -name {CLOCK_50} -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLOCK_50}]
create_clock -name {clk_pll} -period 10.000 -waveform { 0.000 5.000 } [get_nets {pll_inst|sys_sdram_pll_0|sys_pll|altera_pll_i|outclk_wire[0] pll_inst|sys_sdram_pll_0|sys_pll|altera_pll_i|outclk_wire[1]}]
derive_pll_clocks
derive_clock_uncertainty
